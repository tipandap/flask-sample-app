FROM python:3.10-alpine3.15

WORKDIR /flask-sample-app

RUN apk add --no-cache --virtual .build-deps \
        gcc \
        libc-dev \
        libffi-dev \
        openssl-dev \
        make \
    && pip install --no-cache-dir poetry==1.1.13 

COPY . ./

RUN poetry export > requirements.txt \
    && pip3 install --no-cache-dir  -r requirements.txt

EXPOSE 8080

ENTRYPOINT [ "flask" ]

CMD ["run", "--host", "0.0.0.0", "--port", "8080"]
